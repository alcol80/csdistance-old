import sys
import yaml
import logging as log

from timer import Timer

import config
import experiment_engines

def main():
    FORMAT = '%(filename)s:%(lineno)d: [%(funcName)s] %(levelname)s: %(message)s'
    log.basicConfig(level=log.DEBUG, format=FORMAT)

    if len(sys.argv) > 1:
        inputfiles = sys.argv[1:]
    else:
        raise UserWarning('Filename missing')

    res_dict = {}
    for inputfile in inputfiles:
        with open(inputfile, 'r') as exp_file:
            experiment = yaml.load(exp_file)
        log.info('Experiment loaded:\n%s', yaml.dump(experiment))

        results = {}
        res_dict[inputfile] = {}
        for (i_engine, engine) in enumerate(experiment_engines.engines):
            log.info('########## Engine: %s', engine.__class__.__name__)
            with Timer() as ct:
                engine.convert(experiment)
            log.info('Conversion time: %s', ct.msecs)

            results[i_engine] = []
            res_dict[inputfile][i_engine] = []
            for state in experiment.states:
                with Timer() as st:
                    r = engine.solve(state)
                log.info('Solution: %s (%s msec)', r, st.msecs)
                results[i_engine].append([ct.msecs, st.msecs, r])
                res_dict[inputfile][i_engine].append(dict(
                    state=state, engine=repr(engine), conversion_time=ct.msecs,
                    solution_time=st.msecs, distance=r))

        log.info('Results:')
        for (i_engine, rs) in results.iteritems():
            for r in rs:
                print (experiment_engines.engines[i_engine], r)

                # print yaml.dump(res_dict)


if __name__ == '__main__':
    main()
