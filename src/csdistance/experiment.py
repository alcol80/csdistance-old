__author__ = 'axl'

import random as rnd


def out_of(k, n):
    """It randomly succeeds `k` times out of `n`.

    :rtype : bool
    Returns:
      True if the random number generated from [0, `n`) is less than `k`
    """
    return rnd.randrange(n) < k


def dict_map(f, d):
    return dict((f(k, v) for (k, v) in d.iteritems()))


class Variables:
    name_format = 'x{}'
    default_values = dict(vtype='rational', lower_bound=None, upper_bound=None)

    def __init__(self, *args, **kvargs):
        self.variables = {}
        self.init(*args, **kvargs)

    def iteritems(self):
        return ((name, self.get(name)) for name in self.variables.iterkeys())

    @staticmethod
    def empty():
        return Variables()

    # @staticmethod
    # def func_update(d, u):
    # shallow_copy = d.copy()
    #     shallow_copy.update(u)
    #     return shallow_copy

    def init(self, num=0, **kwargs):
        for i in range(num):
            self.set(self.name_format.format(i), **kwargs)

    def names(self):
        return sorted(self.variables.keys())

    def update(self, update_dict):
        for (name, params) in update_dict.iteritems():
            self.set(name, **params)

    def set(self, name, **kwargs):
        new_val = self.variables.get(name, self.default_values).copy()
        new_val.update(kwargs)
        self.variables[name] = new_val
        # self.variables[name] = Variables.func_update(new_val, kwargs)

    def get(self, name):
        return self.variables.get(name, None)

    def length(self):
        return len(self.variables.keys())

    def convert(self, f):
        """It converts a set of variables from one form to another
        @param: `f` is a function with type `(k, v) -> (k', v')`
        """
        new_variables = dict((f(k, v)
                              for (k, v) in self.variables.iteritems()))
        ret = Variables()
        ret.variables = new_variables
        return ret


def coefficient_typed_uniform(vtype):
    if vtype == 'int':
        return rnd.randrange(-20, 20)
    elif vtype == 'rational':
        return float(rnd.randrange(-20, 20))
    elif vtype == 'binary':
        return rnd.randrange(-20, 20)
    else:
        raise ValueError('Wrong type of variable', vtype)


class Constraint:
    @staticmethod
    def typed_uniform(vtype):
        if vtype == 'int':
            return rnd.randrange(-20, 20)
        elif vtype == 'rational':
            return float(rnd.randrange(-20, 20))
        elif vtype == 'binary':
            return rnd.randrange(-20, 20)
        else:
            raise ValueError('Wrong type of variable', vtype)

    @staticmethod
    def generate_random(variables,
                        non_zero_percentage=50.0,
                        rnd_coeff_func=coefficient_typed_uniform,
                        rnd_const_func=lambda: Constraint.typed_uniform('int')):
        n = variables.length()
        k = int(n * non_zero_percentage / 100.0)
        elems = {var: rnd_coeff_func(params['vtype'])
                 for (var, params) in variables.iteritems()
                 if out_of(k, n)}
        constant = rnd_const_func()
        return [elems, constant]


def state_typed_uniform(vtype):
    if vtype == 'int':
        return rnd.randrange(-200, 200)
    elif vtype == 'rational':
        return float(rnd.randrange(-200, 200))
    elif vtype == 'binary':
        return rnd.randrange(-20, 20)
    else:
        raise ValueError('Wrong type of variable', vtype)


class State:
    @staticmethod
    def typed_uniform(vtype):
        if vtype == 'int':
            return rnd.randrange(-200, 200)
        elif vtype == 'rational':
            return float(rnd.randrange(-200, 200))
        elif vtype == 'binary':
            return rnd.randrange(-20, 20)
        else:
            raise ValueError('Wrong type of variable', vtype)

    @staticmethod
    def generate_random(variables,
                        rnd_func=state_typed_uniform):
        return {var: rnd_func(params['vtype'])
                for (var, params) in variables.iteritems()}


class Experiment:
    def __init__(self, descr=None):
        self.descr = descr
        self.variables = Variables.empty()
        self.constraints = []
        self.states = []
        self.generation_time = None

    def generate_variables(self, *args, **kwargs):
        self.variables = Variables(*args, **kwargs)

    def generate_random_constraints(self, num):
        self.constraints = [Constraint.generate_random(self.variables)
                            for _i in range(num)]

    def generate_random_states(self, num):
        self.states = [State.generate_random(self.variables)
                       for _i in range(num)]