from unittest import TestCase
from experiment import Variables

__author__ = 'axl'


class TestVariables(TestCase):
    def test_empty(self):
        ev = Variables.empty()
        self.assertEquals(ev.variables, {})

    def test_init(self):
        v = Variables()
        num = 5
        v.init(num)
        r = {Variables.name_format.format(i): Variables.default_values
             for i in range(num)}
        self.assertEquals(v.variables, r)

    def test_names(self):
        num = 5
        v = Variables(num)
        names = ['x0', 'x1', 'x2', 'x3', 'x4']
        r = v.names()
        self.assertEquals(r, names)
        self.assertEquals(len(r), num)

        w = Variables()
        w.name_format = 'v{}'
        w.init(num)
        names = ['v0', 'v1', 'v2', 'v3', 'v4']
        self.assertEquals(w.names(), names)

    # def test_update(self):
    # self.fail()

    def test_set(self):
        num = 5
        v = Variables(num)
        v.set('x1', vtype='int')
        r = v.get('x1')
        self.assertEquals(r, dict(vtype='int', lower_bound=None, upper_bound=None))

        self.assertEquals(v.get('asdf'), None)
        v.set('asdf')
        r = v.get('asdf')
        self.assertEquals(r, Variables.default_values)

        self.assertEquals(v.get('dummy'), None)
        v.set('dummy', lower_bound=10)
        r = v.get('dummy')
        self.assertEquals(r, dict(vtype='rational', lower_bound=10, upper_bound=None))

    def test_get(self):
        num = 5
        v = Variables(num)
        r = v.get('x1')
        self.assertEquals(r, Variables.default_values)

        r = v.get('asdf')
        self.assertEquals(r, None)

    def test_length(self):
        num = 5
        v = Variables(num)
        self.assertEquals(v.length(), num)
        self.assertEquals(len(v.names()), v.length())

    def test_convert(self):
        def id_var_map(var, params):
            return var, params

        def var_map(var, params):
            new_params = params.copy()
            new_params['LB'] = new_params['lower_bound']
            new_params['UB'] = new_params['upper_bound']
            new_params.pop('lower_bound')
            new_params.pop('upper_bound')
            return var, new_params

        num = 5
        v = Variables(num)
        w = v.convert(id_var_map)
        self.assertEquals(v.variables, w.variables)

        w = v.convert(var_map)
        r = {v: dict(vtype='rational', LB=None, UB=None) for v in v.names()}
        self.assertEquals(w.variables, r)
