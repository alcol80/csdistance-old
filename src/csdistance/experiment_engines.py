import engine.z3e as z3e
import engine.lp as lpe


# engines = [lpe.Pulp(), z3e.Optimize(), z3e.OptimizeAlt(),
# z3e.IterativeBisect(), z3e.IterativeBisectAlt()]
# engines = [z3e.IterativeBisectAlt(), z3e.OptimizeAlt(), z3e.Optimize()]
# engines = [z3e.OptimizeAlt(id_str='1'), z3e.Optimize(id_str='2'),
# z3e.Optimize(id_str='3'), z3e.OptimizeAlt(id_str='4')]
engines = [lpe.Pulp(id_str='0'),
           z3e.OptimizeLinearAlt(id_str='1')]#,
           # z3e.OptimizeLinear(id_str='2')] # ,
           # z3e.IterativeBisectAlt(id_str='1'),
           # z3e.IterativeBisect(id_str='2'),
           # z3e.IterativeBisect(id_str='3'),
           # z3e.IterativeBisectAlt(id_str='4')]