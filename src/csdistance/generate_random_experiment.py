import os
import yaml

from timer import Timer
import experiment
import config

import logging as log


if __name__ == '__main__':
    FORMAT = '%(filename)s:%(lineno)d: [%(funcName)s] %(levelname)s: %(message)s'
    log.basicConfig(level=log.DEBUG, format=FORMAT)

    e = experiment.Experiment(config.description)

    parameters=(('%s_%dv_%dc.yml' % (type_str, num_vars, int(num_vars*ratio)),
                 type_str,
                 num_vars,
                 int(num_vars*ratio))
                for type_str in ['int', 'rational']
                for ratio in [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 5.0]
                for num_vars in [2, 5, 10, 20, 30, 50, 100, 200, 500, 1000])

    for (filename, type_str, num_variables, num_constraints) in parameters:
        if os.path.exists('./%s' % filename):
            log.info('File exists: %s', './%s' % filename)
            continue
        with Timer() as gt:
            e.generate_variables(num_variables, vtype=type_str)
            e.generate_random_constraints(num_constraints)
            e.generate_random_states(config.num_states)
            # e.generate_variables(config.num_variables, vtype=config.vtype)
            # e.generate_random_constraints(config.num_constraints)
            # e.generate_random_states(config.num_states)
        e.generation_time = gt.msecs
        with open(filename, 'w') as f:
            f.write(yaml.dump(e))
            print 'Generated experiment file: %s' % filename
