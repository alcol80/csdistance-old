import logging as log

from unittest import TestCase
from src.csdistance.experiment import out_of

__author__ = 'axl'


class TestOut_of(TestCase):
    def test_out_of(self):
        k = 15
        n = 100
        iterations = 5000
        true_iterations = (1 for i in range(iterations) if out_of(k, n))
        total = 1.0 * sum(true_iterations) / iterations
        ratio = 1.0 * k / n
        epsilon = 0.03
        log.info('out_of %d %d', k, n)
        log.info('total: %s', total)
        log.info('ratio: %s', ratio)
        log.info('ratio-e: %s', ratio - epsilon)
        log.info('ratio+e: %s', ratio + epsilon)
        assert (ratio - epsilon <= total and total <= ratio + epsilon)