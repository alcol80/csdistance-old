import time


class Timer(object):
    def __init__(self, msg=None):
        self.msg = msg

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.msecs = (self.end - self.start) * 1000  # millisecs
        if self.msg is not None:
            print '%s: %f ms' % (self.msg, self.msecs)
