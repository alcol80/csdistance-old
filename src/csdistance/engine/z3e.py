import z3

import logging as log

from csdistance.experiment import Variables
import base


class Base(base.Engine):
    def __init__(self, *args, **kwargs):
        super(Base, self).__init__(*args, **kwargs)
        self.solver = None
        self.variables = Variables.empty()

    def convert(self, experiment):
        ########################################
        def var_map(name, param):
            if param['vtype'] == 'int':
                return (name, z3.Int(name))
            else:
                return (name, z3.Real(name))

        ########################################

        self.variables = experiment.variables.convert(var_map)

        # tmp_solver = z3.Solver()
        for (exp_var, params) in experiment.variables.iteritems():
            (lb, ub) = (params['lower_bound'], params['upper_bound'])
            if lb is not None:
                self.solver.add(self.variables.get(exp_var) >= lb)
                # tmp_solver.add(self.variables.get(exp_var) >= lb)
            if ub is not None:
                self.solver.add(self.variables.get(exp_var) <= ub)
                # tmp_solver.add(self.variables.get(exp_var) <= ub)

        for [elems, constant] in experiment.constraints:
            sigma = z3.Sum([coeff * self.variables.get(var)
                            for (var, coeff) in elems.iteritems()])
            self.solver.add(sigma <= constant)
            # tmp_solver.add(sigma <= constant)

    def distance_expr(self, state):
        def abs_diff(e1, e2):
            return z3.If(e1 >= e2, e1 - e2, e2 - e1)

        return z3.Sum([abs_diff(self.variables.get(var), val)
                       for (var, val) in state.iteritems()])

    def get_distance_value(self, state):
        return self.solver.model().evaluate(self.distance_expr(state))

    @staticmethod
    def numRef_to_builtin(number):
        if isinstance(number, z3.RatNumRef):
            return float(number.as_fraction())
        elif isinstance(number, z3.IntNumRef):
            return number.as_long()


class Optimize(Base):
    def __init__(self, *args, **kwargs):
        super(Optimize, self).__init__(*args, **kwargs)
        self.solver = z3.Optimize()

    def get_distance_value(self):
        r = self.solver.model().evaluate(self.distance_expression)
        return r
        # return self.distance_objective.value()

    def solve(self, state):
        log.debug('state: %s', state)
        s = self.solver
        s.push()
        self.distance_expression = self.distance_expr(state)
        self.distance_objective = s.minimize(self.distance_expression)
        log.debug('s:\n%r', s)
        distance = None
        r = s.check()
        log.debug('m:\n%r', s.model())
        if r == z3.sat:
            distance = self.get_distance_value()
            log.debug('distance: %s (%s)', distance, distance.__class__.__name__)
            # distance = s.model().evaluate(distance_expression)
        s.pop()
        # elif isinstance(distance, z3.ArithRef):
        # if distance.is_int():
        #         distance = int(distance)
        #     elif distance.is_real():
        #         distance = float(distance)
        #     else:
        #         raise ValueError('distance value error')
        if distance is not None:
            return self.numRef_to_builtin(distance)

        return None


class OptimizeLinear(Base):
    def __init__(self, *args, **kwargs):
        super(OptimizeLinear, self).__init__(*args, **kwargs)
        self.solver = z3.Optimize()

    def convert(self, experiment):
        super(OptimizeLinear, self).convert(experiment)
        self.add_aux_variables(experiment.variables)

    def add_aux_variables(self, variables):
        ########################################
        def aux_var_map(name, param):
            if param['vtype'] == 'int':
                return (name, z3.Int('u_%s' % name))
            else:
                return (name, z3.Real('u_%s' % name))

        ########################################

        self.aux_variables = variables.convert(aux_var_map)

    def get_distance_value(self):
        return self.distance_objective.lower()

    def distance_expr(self, state):
        r = z3.Sum(self.aux_variables.variables.values())
        return r

    def solve(self, state):
        log.debug('State: %s', state)
        self.solver.push()
        for (gen_var, si) in state.iteritems():
            ci = self.variables.get(gen_var)
            ui = self.aux_variables.get(gen_var)
            self.solver.add(-ui <= si - ci)
            self.solver.add(si - ci <= ui)

        self.distance_objective = self.solver.minimize(self.distance_expr(state))

        distance = None

        if self.solver.check() == z3.sat:
            distance = self.get_distance_value()
            distance = self.numRef_to_builtin(distance)

        self.solver.pop()
        return distance


class IterativeSimple(Base):
    def __init__(self, *args, **kwargs):
        super(IterativeSimple, self).__init__(*args, **kwargs)
        threshold = kwargs.get('threshold', 0)
        self.id_str += '|(t=%s)' % threshold
        self.solver = z3.Solver()
        self.threshold = threshold

    # def solve(self, state, epsilon=1):
    # s = self.solver
    #     distance_expression = self.distance_expr(state)
    #     distance = None
    #     s.push()
    #     while s.check() == z3.sat:
    #         distance = s.model().evaluate(distance_expression)
    #         s.pop()
    #         s.push()
    #         s.add(distance_expression < distance - epsilon)
    #     s.pop()
    #     if distance is not None and isinstance(distance, z3.RatNumRef):
    #         distance = float(distance.as_fraction())
    #     return distance

    def solve(self, state, epsilon=None):
        s = self.solver
        distance_expression = self.distance_expr(state)

        if s.check() == z3.unsat:
            return None

        distance = s.model().evaluate(distance_expression)
        if isinstance(distance, z3.RatNumRef):
            distance = float(distance.as_fraction())
        log.info('(%s) distance: %s', self.__class__.__name__, distance)

        s.push()
        s.add(distance_expression < distance - self.threshold)
        while s.check() == z3.sat:
            distance = s.model().evaluate(distance_expression)
            if isinstance(distance, z3.RatNumRef):
                distance = float(distance.as_fraction())
            log.info('(%s) distance: %s', self.__class__.__name__, distance)
            s.pop()

            s.push()
            s.add(distance_expression < distance - self.threshold)
        s.pop()

        return distance


class IterativeBisect(Base):
    def __init__(self, *args, **kwargs):
        super(IterativeBisect, self).__init__(*args, **kwargs)
        self.solver = z3.Solver()

    def get_distance_value(self):
        return self.solver.model().evaluate(self.distance_expression)

    def solve(self, state, epsilon=0.5):
        s = self.solver
        self.distance_expression = self.distance_expr(state)

        def go(low, up):
            if up - low <= epsilon:
                return up

            h = (up + low) / 2

            s.push()
            s.add(self.distance_expression < h)
            if s.check() == z3.sat:
                d = self.numRef_to_builtin(self.get_distance_value())
                log.info('(%s) d: %s', self.__class__.__name__, d)
                s.pop()
                return go(low, d)
            else:
                s.pop()
                return go(h, up)

        if s.check() == z3.unsat:
            return None

        distance = self.get_distance_value()
        if isinstance(distance, z3.RatNumRef):
            distance = float(distance.as_fraction())
        log.info('(%s) distance: %s', self.__class__.__name__, distance)

        return go(0, distance)


class BaseAlt(Base):
    def __init__(self, *args, **kwargs):
        super(BaseAlt, self).__init__(*args, **kwargs)
        self.state_variables = Variables.empty()

    def distance_expr(self):
        def abs_diff(e1, e2):
            return z3.If(e1 >= e2, e1 - e2, e2 - e1)

        return z3.Sum([abs_diff(self.variables.get(xi), si)
                       for (xi, si) in self.state_variables.iteritems()])

    def get_distance_value(self):
        return self.solver.model()[self.distance_var]

    def convert(self, experiment):
        ########################################
        def state_var_map(name, param):
            if param['vtype'] == 'int':
                return (name, z3.Int('s_%s' % name))
            else:
                return (name, z3.Real('s_%s' % name))

        ########################################

        super(BaseAlt, self).convert(experiment)
        self.state_variables = experiment.variables.convert(state_var_map)

        rational_found = False
        for v in self.variables.names():
            if self.variables.get(v).is_real():
                rational_found = True
                break
        log.debug('rational_found: %s', rational_found)
        if rational_found:
            self.distance_var = z3.Real('d')
        else:
            self.distance_var = z3.Int('d')

        self.solver.add(self.distance_var == self.distance_expr())
        # self.solver.add(self.distance_var >= 0)

    def add_state_constraints(self, state):
        assert isinstance(state, dict)
        for (xi, vi) in state.iteritems():
            self.solver.add(self.state_variables.get(xi) == vi)


class OptimizeAlt(BaseAlt):
    def __init__(self, *args, **kwargs):
        super(OptimizeAlt, self).__init__(*args, **kwargs)
        self.solver = z3.Optimize()

    def convert(self, experiment):
        super(OptimizeAlt, self).convert(experiment)
        self.distance_objective = self.solver.minimize(self.distance_var)

    def solve(self, state, epsilon=0.0):
        log.debug('state: %s', state)
        self.solver.push()
        self.add_state_constraints(state)
        log.debug('solver:\n%r', self.solver)


        if self.solver.check() == z3.sat:
            distance = self.get_distance_value()
            distance = self.numRef_to_builtin(distance)
        else:
            distance = None

        self.solver.pop()
        return distance


class OptimizeLinearAlt(BaseAlt):
    def __init__(self, *args, **kwargs):
        super(OptimizeLinearAlt, self).__init__(*args, **kwargs)
        self.solver = z3.Optimize()

    def convert(self, experiment):
        self.add_aux_variables(experiment.variables)
        super(OptimizeLinearAlt, self).convert(experiment)
        for (gen_var, ci) in self.variables.iteritems():
            ui = self.aux_variables.get(gen_var)
            si = self.state_variables.get(gen_var)
            self.solver.add(-ui <= si - ci)
            self.solver.add(si - ci <= ui)

        self.distance_objective = self.solver.minimize(self.distance_var)

    def add_aux_variables(self, variables):
        ########################################
        def aux_var_map(name, param):
            if param['vtype'] == 'int':
                return (name, z3.Int('u_%s' % name))
            else:
                return (name, z3.Real('u_%s' % name))

        ########################################

        self.aux_variables = variables.convert(aux_var_map)

    def distance_expr(self):
        r = z3.Sum(self.aux_variables.variables.values())
        return r

    def solve(self, state):
        self.solver.push()
        self.add_state_constraints(state)

        if self.solver.check() == z3.sat:
            distance = self.get_distance_value()
            distance = self.numRef_to_builtin(distance)
        else:
            distance = None

        self.solver.pop()
        return distance


class IterativeSimpleAlt(BaseAlt):
    def __init__(self, *args, **kwargs):
        super(IterativeSimpleAlt, self).__init__(*args, **kwargs)
        self.solver = z3.Solver()

    def solve(self, state, epsilon=0.0):
        log.debug('state: %s', state)
        self.solver.push()
        self.solver.add(self.distance_var >= 0)

        self.add_state_constraints(state)
        log.debug('solver:\n%r', self.solver)

        if self.solver.check() == z3.unsat:
            self.solver.pop()
            return None

        distance = self.solver.model()[self.distance_var]
        log.info('(%s) distance: %s', self.__class__.__name__, distance)
        if isinstance(distance, z3.RatNumRef):
            distance = float(distance.as_fraction())
        elif isinstance(distance, z3.IntNumRef):
            distance = distance.as_long()
        log.info('(%s) distance: %s', self.__class__.__name__, distance)

        self.solver.add(self.distance_var < distance)
        while self.solver.check() == z3.sat:
            distance = self.solver.model()[self.distance_var]
            log.info('(%s) distance: %s (%s)', self.__class__.__name__, distance, distance.__class__.__name__)
            # if isinstance(distance, z3.RatNumRef):
            # distance = float(distance.as_fraction())
            # elif isinstance(distance, z3.IntNumRef):
            #     distance = distance.as_long()
            # log.info('(%s) distance: %s (%s)', self.__class__.__name__, distance, distance.__class__.__name__)
            self.solver.pop()

            self.solver.push()
            self.solver.add(self.distance_var < distance)
        self.solver.pop()

        if isinstance(distance, z3.RatNumRef):
            distance = float(distance.as_fraction())
        elif isinstance(distance, z3.IntNumRef):
            distance = distance.as_long()
        return distance


class IterativeBisectAlt(BaseAlt):
    def __init__(self, *args, **kwargs):
        super(IterativeBisectAlt, self).__init__(*args, **kwargs)
        self.solver = z3.Solver()
        self.epsilon = kwargs.get('epsilon', 0.5)

    def solve(self, state):
        log.debug('state: %s', state)

        # push before adding state constraints `var_state_i = s_i`
        self.solver.push()
        self.add_state_constraints(state)
        log.debug('solver:\n%r', self.solver)

        # Check if there exists a solution, and find a feasible value
        # for the distance `d`
        if self.solver.check() == z3.unsat:
            return None

        # loop invariant:
        # `up` : the last valid distance (not necessarily the minimum one)
        #    `low`: the last lower bound checked, such as no solution exists
        #           for `d < low`. Initial condition is `low` = 0 because `d`
        #           is positive.
        up = self.numRef_to_builtin(self.get_distance_value())
        low = 0

        while (up - low) > self.epsilon:
            # if there is still a significant segment to explore:

            # take the mid point
            h = int(float(up + low) / 2.0 + 0.5)

            # push before adding the constraint on the distance
            self.solver.push()
            self.solver.add(self.distance_var < h)

            # find a solution for `d < h`
            if self.solver.check() == z3.sat:
                # if there is valid value for `d < h`, update `up`
                up = self.numRef_to_builtin(self.get_distance_value())
            else:
                # otherwise h is the new unsatisfiable lower bound
                low = h

            # pop the constraint on the distance
            self.solver.pop()

        # pop state constraints
        self.solver.pop()

        return up
