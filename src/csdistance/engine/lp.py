import pulp

import base


def dict_map(f, d):
    return dict((f(k, v) for (k, v) in d.iteritems()))


class Pulp(base.Engine):
    def __init__(self, *args, **kwargs):
        super(Pulp, self).__init__(*args, **kwargs)

    def convert(self, experiment):
        def var_map(name, params):
            def map_param(k, v):
                if k == 'vtype':
                    cats = {'int': pulp.LpInteger,
                            'rational': pulp.LpContinuous,
                            'binary': pulp.LpBinary}
                    return ('cat', cats[v])
                else:
                    bounds = dict(lower_bound='lowBound', upper_bound='upBound')
                    return (bounds[k], v)

            new_params = dict_map(map_param, params)
            return (name, pulp.LpVariable(name, **new_params))

        self.variables = experiment.variables.convert(var_map)

        self.constraints = [
            pulp.LpAffineExpression([(self.variables.get(var), coeff)
                                     for (var, coeff) in elems.iteritems()]) <= constant
            for [elems, constant] in experiment.constraints
        ]

    def solve(self, state):
        def calculate_distance(problem):
            return pulp.value(problem.objective)

        problem = pulp.LpProblem('Distance from critical states',
                                 pulp.LpMinimize)

        for c in self.constraints:
            problem += c
        ####################
        # add the constraints -u_i <= s_i <= u_i to convert a problem with
        # absolute value to a linear programming problem

        # make a variable 'u_i' for each system component
        us = {gv: pulp.LpVariable(name='u_%s' % lpv, cat=lpv.cat)
              for (gv, lpv) in self.variables.iteritems()}

        # add the constraints
        for (gen_var, si) in state.iteritems():
            (ui, ci) = (us[gen_var], self.variables.get(gen_var))
            problem += -ui <= si - ci
            problem += si - ci <= ui
        ####################
        problem += pulp.LpAffineExpression([(u, 1) for u in us.itervalues()])
        r = problem.solve(pulp.GLPK(msg=0))
        return calculate_distance(problem)


# class LpEngine(Engine):
# def __init__(self, variables=None):
#         def var_map(name, params):
#             def map_param(k, v):
#                 if k == 'vtype':
#                     cats = {'int'      : pulp.LpInteger,
#                             'rational' : pulp.LpContinuous,
#                             'binary'   : pulp.LpBinary}
#                     return ('cat', cats[v])
#                 else:
#                     bounds = dict(lower_bound='lowBound', upper_bound='upBound')
#                     return (bounds[k], v)
#             new_params = dict_map(map_param, params)
#             return (name, pulp.LpVariable(name, **new_params))

#         self.variables = variables.convert(var_map)

#     def init(self, variables):
#         conv_dict = dict(vtype=('cat',
#                                 dict(int=pulp.LpInteger,
#                                      rational=pulp.LpContinuous,
#                                      binary=pulp.LpBinary)),
#                          lower_bound=('lowBound',{}),
#                          upper_bound=('upBound', {}))
#         def var_map(name, params):
#             def map_param(k, v):
#                 if k == 'vtype':
#                     cats = {'int'      : pulp.LpInteger,
#                             'rational' : pulp.LpContinuous,
#                             'binary'   : pulp.LpBinary}
#                     return ('cat', cats[v])
#                 else:
#                     bounds = dict(lower_bound='lowBound', upper_bound='upBound')
#                     return (bounds[k], v)
#             new_params = dict_map(map_param, params)
#             return (name, pulp.LpVariable(name, **new_params))

#     def convert(self, constraint):
#         """Convert a pair (elems, constant) to an LP constraint, given the dict
#         mapping general variables (i.e. strings) to LP variables

#         Args:
#           elems (dict of str*num): the dictionary that associates coefficients
#             to variables
#           constant (num): the constant value of the linear constraint
#           z3_variables (dict of str*z3 symbol): the dictionary that associates
#             a variable name to a z3 variables (symbol).

#         Returns:
#           z3.expr: The Z3 expression representing the linear constraint.
#         """
#         (elems, constant) = constraint
#         lp_affine = pulp.LpAffineExpression([(self.variables.get(var), coeff)
#                                              for (var, coeff) in elems.iteritems()])
#         lp_constraint = lp_affine <= constant
#         # lp_constraint = pulp.LpConstraint(lp_affine, pulp.LpConstraintLE, constant)
#         return lp_constraint

#     def make(self, constraints):
#         self.constraints = map(self.convert, constraints)

#     def solve(self, state):
#         def calculate_distance(problem):
#             return pulp.value(problem.objective)

#         problem = pulp.LpProblem('Distance from critical states',
#                                  pulp.LpMinimize)

#         for c in self.constraints:
#             problem += c
#         ####################
#         # add the constraints -u_i <= s_i <= u_i to convert a problem with
#         # absolute value to a linear programming problem

#         # make a variable 'u_i' for each system component
#         u = {gv: pulp.LpVariable(name='u_%s' % lpv, cat=lpv.cat)
#              for (gv, lpv) in self.variables.iteritems()}

#         # add the constraints
#         for (gen_var, si) in state.iteritems():
#             (ui, ci) = (u[gen_var], self.variables.get(gen_var))
#             problem += -ui <= si - ci
#             problem += si - ci <= ui
#         ####################
#         problem += pulp.LpAffineExpression([(ui, 1) for ui in u.itervalues()])
#         r = problem.solve(pulp.GLPK(msg=0))
#         self.solution = calculate_distance(problem)
