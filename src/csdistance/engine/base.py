class Engine(object):
    def __init__(self, id_str=None, *args, **kwargs):
        self.id_str = id_str

    def __repr__(self):
        if self.id_str is not None:
            return '%s(%s)' % (type(self).__name__, self.id_str)
        else:
            return type(self).__name__
